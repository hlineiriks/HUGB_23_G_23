import unittest

from interface import TravelInterface


# TODO: to run tests: python3 -m unittest tests/test_place.py
from models.place import Place, Location, Direction


class TestPlace(unittest.TestCase):
    def setUp(self) -> None:
        self.place1 = Place(
            id=1,
            name="Hotel transylvania",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=(23.6345, Direction.NORTH),
                longitude=(102.5528, Direction.WEST),
            ),
        )
        self.place2 = Place(
            id=1,
            name="Hotel transylvania",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=(23.6345, Direction.NORTH),
                longitude=(102.5528, Direction.WEST),
            ),
        )
        self.place3 = Place(
            id=11,
            name="Worldfit Kringlan",
            description="Gym for crazy people",
            location=Location(
                latitude=(64.07504, Direction.NORTH),
                longitude=(21.53495, Direction.WEST),
            ),
        )

        return super().setUp()

    def test_place_eq_correct(self):
        self.assertEqual(self.place1, self.place2)

    def test_place_eq_not_correct(self):
        self.assertNotEqual(self.place1, self.place3)

    def test_place_eq_not_same_object(self):
        self.assertNotEqual(self.place1, ("fake", "place"))

    def test_place_location_eq_not_same_object(self):
        self.assertNotEqual(
            Location(
                latitude=(64.07504, Direction.NORTH),
                longitude=(21.53495, Direction.WEST),
            ),
            (4, 5),
        )


class TestTravelInterface(unittest.TestCase):
    def setUp(self):
        self.interface = TravelInterface()

    def test_get_place_by_id(self):
        place = self.interface.get_place(1)
        expected_ret_place = Place(
            id=1,
            name="Hotel transylvania",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=(23.6345, Direction.NORTH),
                longitude=(102.5528, Direction.WEST),
            ),
        )
        self.assertIsNotNone(place)
        self.assertEqual(place, expected_ret_place)

    def test_get_place_by_invalid_id(self):
        place = self.interface.get_place(80)
        self.assertIsNone(place)

    def test_create_place(self):
        expected_place = Place(
            id=2,
            name="Worldfit Kringlan",
            description="Gym for crazy people",
            location=Location(
                latitude=(64.07504, Direction.NORTH),
                longitude=(21.53495, Direction.WEST),
            ),
        )
        place = self.interface.add_place(
            "Worldfit Kringlan",
            "Gym for crazy people",
            Location(
                latitude=(64.07504, Direction.NORTH),
                longitude=(21.53495, Direction.WEST),
            ),
        )
        self.assertEqual(place, expected_place)

    def test_get_all_places(self):
        all_places = self.interface.get_all_places()
        self.assertEqual(
            all_places,
            [
                Place(
                    id=1,
                    name="Hotel transylvania",
                    description="Bloodie vampiros de lagoon",
                    location=Location(
                        latitude=(23.6345, Direction.NORTH),
                        longitude=(102.5528, Direction.WEST),
                    ),
                ),
                Place(
                    id=2,
                    name="Worldfit Kringlan",
                    description="Gym for crazy people",
                    location=Location(
                        latitude=(64.07504, Direction.NORTH),
                        longitude=(21.53495, Direction.WEST),
                    ),
                ),
            ],
        )
    
    def test_z_edit_place(self):
        '''This test is last (z) because it changes the places.json file
        and the other tests depend on the data in the file.'''
        expected_place = Place(
            id=1,
            name="Disney Land",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=(23.6345, Direction.NORTH),
                longitude=(102.5528, Direction.WEST),
            ),
        )
        place = self.interface.edit_place(
            1,
            "Disney Land",
            "Bloodie vampiros de lagoon",
            Location(
                latitude=(23.6345, Direction.NORTH),
                longitude=(102.5528, Direction.WEST),
            ),
        )
        self.assertEqual(place, expected_place)