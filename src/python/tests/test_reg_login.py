import unittest

from registration_login import UserManager

class TestUserManager(unittest.TestCase):
    def setUp(self):
        self.user_manager = UserManager()

    def test_register(self):
        # Test registration success
        self.assertEqual(self.user_manager.register('user1', 'password1'), 'Registration successful!')

        # Test registration with already existing username
        self.assertEqual(self.user_manager.register('user1', 'password2'), 'Username already exists. Please choose a different username.')

    def test_login(self):
        # Login before registration should fail
        self.assertEqual(self.user_manager.login('user1', 'password1'), 'Invalid login details')

        # Register a user
        self.user_manager.register('user1', 'password1')
        
        # Test successful login
        self.assertEqual(self.user_manager.login('user1', 'password1'), 'Login successful!')

        # Test unsuccessful login due to wrong password
        self.assertEqual(self.user_manager.login('user1', 'wrongpassword'), 'Invalid login details')

if __name__ == '__main__':
    unittest.main()
