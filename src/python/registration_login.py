import getpass
from typing import Dict

class UserManager:
    """
    A simple class for user management, allowing user registration and login.
    """
    
    def __init__(self):
        """
        Initializes a new instance of the UserManager class, setting up an empty user database.
        """
        self.users_db: Dict[str, str] = {}
        
    def register(self, username: str, password: str) -> str:
        """
        Registers a new user with the given username and password.
        """
        if username in self.users_db:
            return "Username already exists. Please choose a different username."
        self.users_db[username] = password
        return "Registration successful!"
        
    def login(self, username: str, password: str) -> str:
        """
        Logs in a user with the given username and password.
        """
        if username in self.users_db and self.users_db[username] == password:
            return "Login successful!"
        else:
            return "Invalid login details"

def main():
    """
    The main function of the script, which initializes a UserManager instance and prompts the user
    to either register a new user, log in an existing user, or exit the application.
    """
    user_manager = UserManager()
    while True:
        action = input("Do you want to login or register? (login/register/exit): ").lower()
        if action == 'exit':
            break
        username = input("Enter your username: ")
        password = getpass.getpass("Enter your password: ")

        if action == 'login':
            print(user_manager.login(username, password))
        elif action == 'register':
            print(user_manager.register(username, password))
        else:
            print("Invalid action. Please enter 'login' or 'register' or 'exit'.")

if __name__ == "__main__":
    """
    If this script is the main module, call the main() function to execute the application logic.
    """
    main()
