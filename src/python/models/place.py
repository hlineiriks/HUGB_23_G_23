from enum import Enum


class Direction(Enum):
    NORTH = "N"
    EAST = "E"
    WEST = "W"
    SOUTH = "S"


class Location:
    def __init__(
        self, latitude: tuple[float, Direction], longitude: tuple[float, Direction]
    ):
        self.latitude = latitude
        self.longitude = longitude

    def __eq__(self, __value: object) -> bool:
        if isinstance(__value, Location):
            return (
                self.latitude == __value.latitude
                and self.longitude == __value.longitude
            )
        return False
    
    def __str__(self):
        return f"Location: {self.latitude} - {self.longitude}"
    
    def __repr__(self):
        return self.__str__()


class Place:
    def __init__(self, id: int, name: str, description: str, location: Location):
        self.id = id
        self.name = name
        self.description = description
        self.location = location

    def __eq__(self, __value: object) -> bool:
        if isinstance(__value, Place):
            # Compare all attributes for equality
            return (
                self.id == __value.id
                and self.name == __value.name
                and self.description == __value.description
                and self.location == __value.location
            )
        return False
    
    def __str__(self):
        return f"Place {self.id}: {self.name} - {self.description} - {self.location.latitude} - {self.location.longitude}"
    
    def __repr__(self):
        return self.__str__()
