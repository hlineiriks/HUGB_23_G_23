from models.place import Place, Location, Direction
import json
import os
from typing import List
from typing import Optional

class TravelInterface:
    def __init__(self):
        # Going up one level from the current file and then down to the data folder
        data_folder = os.path.join(os.path.dirname(__file__), '..', 'data')
        self.places_file = os.path.join(data_folder, 'places.json')
        # Initialize places list with items from the json file
        self.places: List[Place] = []
        self.load_places()

    def load_places(self):
        '''Opens the places.json file and loads the data into the places list'''
        try:
            with open(self.places_file, 'r', encoding='utf-8') as file:
                data = json.load(file)
                for place_data in data:
                    # Create a Place object from the data
                    place = Place(
                        id=place_data['id'],
                        name=place_data['name'],
                        description=place_data['description'],
                        location=Location(
                            latitude=(place_data['latitude']['value'], Direction(place_data['latitude']['direction'])),
                            longitude=(place_data['longitude']['value'], Direction(place_data['longitude']['direction']))
                        )
                    )
                    # Append the Place object to the places list
                    self.places.append(place)
        except FileNotFoundError:
            raise Exception("The 'places.json' file could not be found.")
    
    def update_places_json(self):
        '''Rewrites the places.json file with the data from the places list'''
        # Convert the list of Place objects to a list of dictionaries
        places_data = []
        for place in self.places:
            place_data = {
                "id": place.id,
                "name": place.name,
                "description": place.description,
                "latitude": {
                    "value": place.location.latitude[0],
                    "direction": place.location.latitude[1].value,
                },
                "longitude": {
                    "value": place.location.longitude[0],
                    "direction": place.location.longitude[1].value,
                },
            }
            places_data.append(place_data)

        # Write the updated data to the json file
        with open(self.places_file, 'w', encoding='utf-8') as file:
            json.dump(places_data, file, indent=4)

    def get_place(self, place_id: int = 0):
        '''Returns a Place object corresponding to the ID given as a parameter,
        otherwise returns None'''
        for place in self.places:
            if place.id == place_id:
                return place
        return None

    def add_place(self, name: str, description: str, location: Location):
        '''Generates an id, creates a new Place object with given parameter 
        values and appends the Place object to the places list'''
        new_id = len(self.places) + 1
        new_place = Place(new_id, name, description, location)
        self.places.append(new_place)
        self.update_places_json()
        return new_place

    def get_all_places(self):
        '''Returns the places list'''
        return self.places

    def delete_place(self, place_id: int):
        '''Deletes a place by its ID from the places list (created within the class) and the JSON file'''

        place_to_delete = None # Initialize a variable to store the place to be deleted
        for place in self.places:
            if place.id == place_id: # Check if the place's ID matches the given ID
                place_to_delete = place # Set the place to be deleted
                break # Exit the loop if we found the place to delete

        if place_to_delete:
            # Remove the place from the places list
            self.places.remove(place_to_delete)
            self.update_places_json()  # Update the JSON file after removing the place
            print(f"Place with ID {place_id} has been deleted.")
        else:
            print(f"No place found with ID {place_id}. Nothing deleted.")
    
    def edit_place(self, place_id: int, name: Optional[str] = None, description: Optional[str] = None, location: Optional[Location] = None):
        '''Edits the Place object with the given ID (if it exists) and updates the places.json file
        Returns an exception if the place does not exist, otherwise the updated place'''
        place = self.get_place(place_id)
        if place is None:
            raise Exception(f"Place with id {place_id} does not exist.")
        if name is not None:
            place.name = name
        if description is not None:
            place.description = description
        if location is not None:
            place.location = location
        self.update_places_json()
        return place